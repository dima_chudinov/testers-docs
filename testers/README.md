# Установка платформы для Тестеров сети NB-IoT 

### Требования к ПО
1. PostgreSQL 11 и выше или docker-compose
2. Docker

## Варианты установки

1. Docker compose
2. Docker + PostgreSQL инстанс

## Вариант 1. Установка с помощью docker-compose

1. Авторизируйтесь в gitlab container registry с предоставленным доступом
    ```bash
    docker login registry.gitlab.com -u username -p token
    ```
2. Сохраните файл docker-compose
   ```bash
    wget https://gitlab.com/dima_chudinov/testers-docs/-/raw/main/testers/docker-compose.yml
   ```
3. Запустите систему
   ```bash
    docker-compose up -d
   ```
Система будет работать на порту 8080. В случае необходимости сменить порт измените параметр по следующиему пути api > ports > первый порт. Пример, изменить порт на 8082. Должно получиться:
```yml
api:
    ports:
        - 8082:8080
```

## Вариант 2. Установка с использованием поднятой СУБД PostgreSQL

### Подготовка Базы Данных (PostgreSQL)
1. Создайте базу данных
   ```sql
    CREATE DATABASE testers;
   ```
2. Создайте пользователя
   ```sql
    CREATE USER testers WITH PASSWORD 'your_password';
   ```
3. Предоставьте пользователю доступ к созданной базе данных
    ```sql
    GRANT ALL PRIVILEGES ON DATABASE testers TO testers;
   ```

### Запуск docker-контейнера
1. Авторизируйтесь в gitlab container registry с предоставленным доступом
    ```bash
    docker login registry.gitlab.com -u <username> -p <token>
    ```
2. Скачайте docker образ
   ```bash
    docker pull registry.gitlab.com/redbees/rosseti/testers/backend:latest
    ```
3. Запустите docker образ подставив нужные значения в переменные окружения
    ```bash
    docker run -d \
        -p 9090:8080 \
        -e POSTGRES_HOST=localhost \
        -e POSTGRES_USER=testers \
        -e POSTGRES_PASSWORD=your_password \
        -e POSTGRES_DATABASE=testers \
        -e POSTGRES_PORT=5432 \
        -e POSTGRES_SSL=false \
        -e HTTP_PORT=8080 \
        registry.gitlab.com/redbees/rosseti/testers/backend:latest
    ```

## Создание конфигурации для устройства

Создать конфигурацию можно с использованием [SCEF-Tuner](https://sceftuner.redbees.ru/ "SCEFTuner")

1. В качестве URL для получения сообщений от SCEF укажите
    ```
    http://ip_address:port/api/v1/messages
    ```

## Тестирование приложения после установки

Выполнить curl запрос для эмуляции получения данных от SCEF. Указать необходимые ip и port.

```sh
curl -X POST -H "Content-Type: application/json" \
    -d '{"externalId":"dev02@test.ru","niddConfiguration":"/3gpp-nidd/v1/ApplicationID/configurations/603djkfsbdhbf34bdbhf","data":"eydhdHRyaWJ1dGVzJzp7J2ltc2knOicyNTAwMTUxNDAwMDE2MDcnfSwndGVsZW1ldHJ5Jzp7J2JhdHQnOjcsJ3RlbXAnOjE2LCdsYXQnOjU5LjkzMTEsJ2xuZyc6MzAuMzYwOSwnc3RhdHVzJzoxLCdyc3NpJzotNjMsJ29wZXInOjI1MDAxLCdlYXJmY24nOjYyOTMsJ2NlbGxpZCc6JzUxMjY5NjQwJywncnNycCc6LTY3LCdyc3JxJzotMTUuNX19","reliableDataService":false,"niddDownlinkDataTransfer":"","deliveryStatus":""}' \ 
    http://ip:port/api/v1/messages
```

Если тестирование происходит на машине, где развернута система, то можно использовать следующую команду

```sh
curl -X POST -H "Content-Type: application/json" \
    -d '{"externalId":"dev02@test.ru","niddConfiguration":"/3gpp-nidd/v1/ApplicationID/configurations/603djkfsbdhbf34bdbhf","data":"eydhdHRyaWJ1dGVzJzp7J2ltc2knOicyNTAwMTUxNDAwMDE2MDcnfSwndGVsZW1ldHJ5Jzp7J2JhdHQnOjcsJ3RlbXAnOjE2LCdsYXQnOjU5LjkzMTEsJ2xuZyc6MzAuMzYwOSwnc3RhdHVzJzoxLCdyc3NpJzotNjMsJ29wZXInOjI1MDAxLCdlYXJmY24nOjYyOTMsJ2NlbGxpZCc6JzUxMjY5NjQwJywncnNycCc6LTY3LCdyc3JxJzotMTUuNX19","reliableDataService":false,"niddDownlinkDataTransfer":"","deliveryStatus":""}' \ 
    http://localhost:8080/api/v1/messages
```
