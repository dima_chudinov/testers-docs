# Установка платформ для компании ПАО «Россети Сибири»

### Требования к ПО
1. PostgreSQL 11 и выше или docker-compose
2. Docker

## Варианты установки

1. Docker compose
2. Docker + PostgreSQL инстанс

## [Установка платформы для умных счётчиков](https://gitlab.com/dima_chudinov/testers-docs/-/tree/main/counters "Документация по установке")

## [Установка платформы для тестеров сети NB-IoT](https://gitlab.com/dima_chudinov/testers-docs/-/tree/main/testers "Документация по установке")